package nl.wur.ssb.proteinAnnotation;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public class CommandParser {

  public static CommandLine argsValidator(String[] args) throws Exception {
    Options options = new Options();

    Option option = Option.builder("i").argName("file").desc("Fasta file input").hasArg(true)
        .required(true).longOpt("input").build();
    options.addOption(option);

    option = Option.builder("o").argName("file").hasArg(true).desc("Output filename").required(true)
        .longOpt("output").build();
    options.addOption(option);


    option = Option.builder("h").hasArg(false).desc("Print this message").required(false)
        .longOpt("help").build();
    options.addOption(option);

    CommandLineParser parser = new DefaultParser();
    try {
      CommandLine cmd = parser.parse(options, args);
      return cmd;
    } catch (Exception e) {
      System.out.println(e);
      HelpFormatter formatter = new HelpFormatter();
      formatter.printHelp("Missing arguments, possible options see below", options);
    }
    System.exit(0);
    return null;

  }
}
