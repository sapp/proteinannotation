package nl.wur.ssb.proteinAnnotation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.rdfhdt.hdt.exceptions.ParserException;

import nl.wur.ssb.HDT.HDT;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.data.Domain;
import nl.wur.ssb.RDFSimpleCon.data.Property;
import nl.wur.ssb.RDFSimpleCon.data.RDFSubject;
import nl.wur.ssb.RDFSimpleCon.data.RDFType;

public class App {
  private static CommandLine arguments;
  private static RDFSimpleCon SAPPSource;

  public static void main(String[] args) throws Exception

  {
    arguments = CommandParser.argsValidator(args);
    Logger.getRootLogger().setLevel(Level.OFF);
    loadRDF();
    deletePreviousProteinAnnotation();
    proteinAnnotation();
    save();
  }

  private static void deletePreviousProteinAnnotation() throws Exception {
    // Removes previous annotation performed by previous annotation runs.
    // Does not remove original genbank annotation
    SAPPSource.runUpdateQuery("removePreviousannotation.txt");
  }

  private static void proteinAnnotation() throws Exception {
    Domain domain = new Domain(SAPPSource);
    new Property(domain, "ssb:product");
    new RDFType(domain, "ssb:Protein");

    List<Protein> proteinList = new ArrayList<Protein>();
    String protein = "";
    Protein proteinX = null;
    List<ProteinDomain> domainList = new ArrayList<ProteinDomain>();
    for (ResultLine item : SAPPSource.runQuery("getProteinInformation.txt", true)) {
      if (!protein.equals(item.getIRI("protein"))) {
        proteinX = new Protein();
        proteinX.setProteinScore(0);
        proteinX.setProteinName("Hypothetical protein");
      }
      protein = item.getIRI("protein");

      String interpro = item.getIRI("interpro");
      String idesc = item.getLitString("idesc");
      // System.out.println(idesc);
      String sdesc = item.getLitString("sdesc");
      String score = item.getLitString("score");

      ProteinDomain domainX = new ProteinDomain();
      proteinX.addProteinURI(protein);

      proteinX.addDomain(domainX);

      domainX.addDomainURI(interpro);
      domainX.setIDescription(idesc);
      domainX.setSDescription(sdesc);
      domainX.setScore(score);

      proteinList.add(proteinX);

      domainList.add(domainX);
    }

    for (int i = 0; i < proteinList.size(); i++) {
      // System.out.println("#" + proteinList.get(i).getProteinName());
      List<ProteinDomain> domains = proteinList.get(i).getDomains();
      functionCalculator(domains, proteinList.get(i));
      // System.out.println("$" + proteinList.get(i).getProteinName());
    }

    for (int i = 0; i < proteinList.size(); i++) {
      String proteinURI = proteinList.get(i).getProteinURI();
      String product = proteinList.get(i).getProteinName();
      RDFSubject proteinRDF = new RDFSubject(domain, proteinURI, "ssb:Protein");
      proteinRDF.addLit("ssb:product", product.replace("_", " "));
    }
  }

  private static void functionCalculator(List<ProteinDomain> domains, Protein protein) {
    int pscore = protein.getProteinScore();

    // Priority naming... based on local knowledge
    String[] no_priority = {"p-loop", "domain-like", "conserved", "binding", "like", "family",
        "domain", "enzyme", "containing", "unknown", "uncharacterized", "UPF", "(duf)", "terminal",
        "helix-turn-helix", "repeat", "coil"};
    String[] low_priority = {"predicted", "homolog", "putative", "related", "associated"};
    String[] higher_priority_spores = {"spore", "spoI", "sporulation", "germination", "capsule"};
    String[] higher_priority_virus = {"virus", "viral", "phage", "transposase", "transposon",
        "transposition", "integrase", "resolvase", "dde superfamily endonuclease"};
    String[] higher_priority_transporter = {"transport", "influx", "efflux", "permease", "intake",
        "uptake", "symport", "antiport", "import", "pump", "exchanger", "channel", "translocase"};
    String[] higher_priority_transcription_factors =
        {"regul", "repress", "transcription", "zinc-finger", "zinc finger"};
    String[] higher_priority_chaperones =
        {"chaperone", "chaperonin", "heat shock", "heat-shock", "cold-shock", "cold shock"};
    for (ProteinDomain domain : domains) {
      String idescription = domain.getIDescription();
      String sdescription = domain.getSDescription();
      // System.out.println(idescription);
      // System.out.println(sdescription);
      int iscore = 0;

      if (idescription != null) {
        if (idescription.equals(""))
          iscore = -10;
        idescription = idescription.toLowerCase().replace(",", " ");

        iscore = matching(idescription, no_priority, -2, iscore);
        iscore = matching(idescription, low_priority, -1, iscore);
        iscore = matching(idescription, higher_priority_spores, +1, iscore);
        iscore = matching(idescription, higher_priority_virus, +1, iscore);
        iscore = matching(idescription, higher_priority_transporter, +1, iscore);
        iscore = matching(idescription, higher_priority_transcription_factors, +1, iscore);
        iscore = matching(idescription, higher_priority_chaperones, +1, iscore);
      }
      int sscore = 0;
      if (sdescription != null) {
        if (sdescription.equals(""))
          sscore = -10;
        sdescription = sdescription.toLowerCase().replace(",", " ");

        sscore = matching(sdescription, no_priority, -2, sscore);
        sscore = matching(sdescription, low_priority, -1, sscore);
        sscore = matching(sdescription, higher_priority_spores, +1, sscore);
        sscore = matching(sdescription, higher_priority_virus, +1, sscore);
        sscore = matching(sdescription, higher_priority_transporter, +1, sscore);
        sscore = matching(sdescription, higher_priority_transcription_factors, +1, sscore);
        sscore = matching(sdescription, higher_priority_chaperones, +1, sscore);
      }
      if (iscore >= sscore && idescription != null) {
        if (iscore >= pscore) {
          protein.setProteinName(idescription);
        }
      } else if (sscore >= iscore && sdescription != null) {
        if (sscore >= pscore) {
          protein.setProteinName(sdescription);
        }
      }
    }
  }

  private static int matching(String description, String[] elements, int value, int score) {
    for (String element : elements) {
      if (description.contains(element))
        score += value;
    }
    return score;
  }

  private static void save() throws IOException, ParserException {
    HDT hdt = new HDT();
    hdt.save(SAPPSource, arguments.getOptionValue("output"));
  }

  private static void loadRDF() throws Exception {
    String ntfile = File.createTempFile("rdffile", "nt").getAbsolutePath();
    HDT hdt = new HDT();
    hdt.hdt2rdf(arguments.getOptionValue("input"), ntfile);
    SAPPSource = new RDFSimpleCon("file://" + ntfile + "{" + "nt" + "}");
    SAPPSource.setNsPrefix("ssb", "http://csb.wur.nl/genome/");
    SAPPSource.setNsPrefix("protein", "http://csb.wur.nl/genome/protein/");
    SAPPSource.setNsPrefix("biopax", "http://www.biopax.org/release/bp-level3.owl#");
  }
}
