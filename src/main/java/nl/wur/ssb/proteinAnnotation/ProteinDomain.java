package nl.wur.ssb.proteinAnnotation;

public class ProteinDomain {
  private String DomainURI;
  private String IDescription;
  private String SDescription;
  private String Score;

  public void addDomainURI(String DomainURI) {
    this.setDomainURI(DomainURI);
  }

  public String getDomainURI() {
    return DomainURI;
  }

  public void setDomainURI(String domainURI) {
    this.DomainURI = domainURI;
  }

  public String getSDescription() {
    return SDescription;
  }

  public void setSDescription(String sDescription) {
    this.SDescription = sDescription;
  }

  public String getIDescription() {
    return IDescription;
  }

  public void setIDescription(String iDescription) {
    this.IDescription = iDescription;
  }

  public String getScore() {
    return Score;
  }

  public void setScore(String score) {
    this.Score = score;
  }

}
