package nl.wur.ssb.proteinAnnotation;

import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {

  public void testApp() throws Exception {
    String[] args = {"-input", "./src/test/resources/interpro.hdt", "-output",
        "./src/test/resources/galaxy.hdt"};
    App.main(args);
    assertTrue(true);
  }
}
